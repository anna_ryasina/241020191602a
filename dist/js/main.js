(function () {

    "use strict";

    function formatDate(date) {
        var monthNames = [
            "Январь", "Февраль", "Март",
            "Апрель", "Май", "Июнь", "Июль",
            "Август", "Сентябрь", "Октябрь",
            "Ноябрь", "Декабрь"
            ],
            day = date.getDate(),
            monthIndex = date.getMonth(),
            year = date.getFullYear();
        return day + ' ' + monthNames[monthIndex] + ' ' + year;
    }

    function addComment() {
        var valid = validate($('.js_validate'));
        if (valid) {
            var val = $('.js-get-val').val(),
                author = 'Anonym',
                date = formatDate(new Date());
            $('.comments-list').append(
                "<div class='comments-list__item'>" +
                "<div class='comments-list__item-top'>" +
                "<div class='comments-list__author'>"+ author +"</div>" +
                "<div class='comments-list__date'>"+ date +"</div>" +
                "</div>" +
                "<div class='comments-list__cont'>" + val + "</div>" +
                "</div>"
            );
            $('.js-get-val').val('');
        }
    }

    $('.js-get-val').keydown(function (e) {
        if (e.ctrlKey && e.keyCode == 13) {
            addComment();
        }
    });

    $(document).on("click", ".js-add-comment", function () {
        addComment();
        return false;
    });

    /*Function validate*/
    function validate(form) {
        var error_class = "error";
        var norma_class = "pass";
        var e = 0;
        var reg = undefined;

        function mark(object, expression) {
            if (expression) {
                object.parents('.input-group').addClass(error_class).removeClass(norma_class);
                e++;
            } else
                object.parents('.input-group').addClass(norma_class).removeClass(error_class);
        }

        form.find("[required]").each(function () {
            switch ($(this).attr("data-validate")) {
                case undefined:
                    mark($(this), $.trim($(this).val()).length === 0);
                    break;
                default:
                    reg = new RegExp($(this).attr("data-validate"), "g");
                    mark($(this), !reg.test($.trim($(this).val())));
                    break;
            }
        });

        if (e == 0) {
            return true;
        } else {
            form.find("." + error_class + " input:first").focus();
            return false;
        }
    }

    /*Function validate end*/


})();