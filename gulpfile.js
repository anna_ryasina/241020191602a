var gulp = require('gulp'),
    watch = require('gulp-watch'),
    autoprefixer = require('gulp-autoprefixer'),
    jshint = require('gulp-jshint'),
    stylish = require('jshint-stylish'),
    sass = require('gulp-sass'),
    rename = require("gulp-rename"),
    minifyCss = require('gulp-minify-css'),
    uglify = require('gulp-uglify'),
    imagemin = require('gulp-imagemin'),
    concat = require('gulp-concat'),
    plumber = require('gulp-plumber'),
    jade = require('gulp-jade'),
    browserSync = require('browser-sync'),
    reload      = browserSync.reload;


gulp.task('browser-sync', function() {
    browserSync({
        server: {
            baseDir: 'dist'
        },
        notify: false
    })
});

// Jade
gulp.task('jade', function(){
    gulp.src('app/*.jade')
        .pipe(jade({pretty: true}))
        .pipe(gulp.dest('./dist/'))
        .pipe(reload({stream:true}));
});

// перебрасываем папку sass в dist
gulp.task('sass-dist', function () {
    return gulp.src('app/sass/**/*.scss')
        .pipe(gulp.dest('dist/sass/'))
        .pipe(reload({stream:true}));
});

// компилируем sass html
gulp.task('sass', function () {
    return gulp.src('app/sass/**/*.scss')
        .pipe(plumber())
        .pipe(sass())
        .pipe(autoprefixer({
            browsers: ['last 10 versions'],
            cascade: false
        }))
        .pipe(gulp.dest('dist/css/'))
        .pipe(minifyCss())
        .pipe(rename({suffix: ".min",}))
        .pipe(gulp.dest('dist/css/'))
        .pipe(reload({stream:true}));
});

// собираем шрифты
gulp.task('fonts', function() {
    return gulp.src('app/css/fonts/**/*')
        .pipe(gulp.dest('dist/css/fonts'))
        .pipe(reload({stream:true}));
});

// собираем картинки
gulp.task('images', function(){
    return gulp.src('app/images/**/*.*')
        .pipe(imagemin())
        .pipe(gulp.dest('dist/images'))
        .pipe(reload({stream:true}));
    return gulp.src('app/images/favicon/*.*')
        .pipe(imagemin())
        .pipe(gulp.dest('dist/images/favicon/'))
        .pipe(reload({stream:true}));
});
gulp.task('images-content', function(){
    return gulp.src('app/assets/images/**/*.*')
        .pipe(imagemin())
        .pipe(gulp.dest('dist/assets/images'))
        .pipe(reload({stream:true}));
});

// проверяем js на наличие ошибок
gulp.task('jshint', function(){
    return gulp.src('app/js/*.js')
        .pipe(jshint())
        .pipe(jshint.reporter(stylish))
});

//  собираем библиотеки js(желательно min)
gulp.task('concat', function () {
    return gulp.src(
        ['node_modules/jquery/dist/jquery.min.js',]
    )
        .pipe(concat("lib.min.js"))
        .pipe(gulp.dest('dist/js/'));
});

// собираем наш кастомный js
gulp.task('js',function(){
    gulp.src('app/js/main.js')
        .pipe(plumber())
        .pipe(gulp.dest('dist/js/'))
        .pipe(uglify())
        .pipe(rename("main.min.js"))
        .pipe(gulp.dest('dist/js/'))
        .pipe(reload({stream:true}));
});


// наблюдаем за изменениями
gulp.task('watch', function(){
    gulp.watch('app/*.jade',['jade']);
    gulp.watch('app/sass/*.scss', ['sass-dist','fonts','sass'] );
    gulp.watch('app/assets/images/**/*.*', ["images-content"]);
    gulp.watch('app/images/*.*', ["images"]);
    gulp.watch('app/js/**/*.js',  ["jshint", "concat", "js"]);
});

// Default
gulp.task('default', ['jade',"sass-dist","sass",'fonts',"jshint",'concat','js','images','images-content', 'browser-sync', "watch"]);//'browserSync'